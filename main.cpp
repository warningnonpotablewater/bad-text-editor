#include <editor.hpp>

int main(int argc, char **argv) {
	gtk_init(&argc, &argv);
	
	GtkFileChooserNative *dialog = gtk_file_chooser_native_new(
		"Open file", NULL, GTK_FILE_CHOOSER_ACTION_OPEN, "Open", "Cancel"
	);
	
	int response = gtk_native_dialog_run(GTK_NATIVE_DIALOG(dialog));
	
	if (response == GTK_RESPONSE_ACCEPT) {
		edit_file(gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(dialog)));
	}
}
